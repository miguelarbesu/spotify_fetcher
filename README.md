# Spotify fetcher

A script to fetch your playlists, favorites and other collections from Spotify.

Based on [Rinze's `spotify_fix_favorite`](https://codeberg.org/rinze/spotify_fix_favorites).
