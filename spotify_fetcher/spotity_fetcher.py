from collections import defaultdict
import spotipy # type: ignore
from spotipy.oauth2 import SpotifyOAuth # type: ignore
import logging

#### CONSTANTS ####
# If an album has less than these songs, we don't care about it
# MIN_SONGS = 4
# Number of favorite songs / total songs ratio needed to ask the user if we 
# should process this album.
# FAVORITE_TO_SONGS_RATIO = 0.8

class SpotifyClient:

    def __init__(self):
        self._spotify = spotipy.Spotify(
            auth_manager = SpotifyOAuth(
                scope = "user-library-modify"
            )
        )

    def get_favorite_info(self):
        """Returns the number of favorite songs per album and the number of 
        total tracks per album, as we have that info already in this API call.
        """

        offset = 0
        favorite_songs = defaultdict(list)
        tracks_in_album = dict()
        n_tracks = 0
        keep_reading = True
        logging.info("Retrieving your favorite songs. Might take a while if you have a lot...")
        while keep_reading:
            res = self._spotify.current_user_saved_tracks(offset = offset)
            for track in res['items']:
                track_info = track['track']
                logging.info(track_info)
                artist_info = ", ".join(x['name'] for x in track_info['album']['artists'])
                album_key = track_info['album']['id'], track_info['album']['name'], artist_info
                track_key = track_info['id']
                favorite_songs[album_key].append(track_key)
                n_tracks += 1
                tracks_in_album[track_info['album']['id']] = track_info['album']['total_tracks']
            total = res['total']
            keep_reading = n_tracks < total
            offset = n_tracks
        return favorite_songs, tracks_in_album

    # def remove_songs_from_favorites(self, song_id_list):
    #     # Note: some albums have a lot of tracks and this crashes if we send the 
    #     # entire list at once. Sending in smaller chunks works.
    #     CHUNK_SIZE = 10
    #     for idx in range(0, len(song_id_list), CHUNK_SIZE):
    #         self._spotify.current_user_saved_tracks_delete(song_id_list[idx:(idx+CHUNK_SIZE)])

if __name__ == "__main__":

    sc = SpotifyClient()
    all_favorite_songs, tracks_in_album = sc.get_favorite_info()
    for album_key, favorited_tracks in all_favorite_songs.items():
        album_id, album_name, artists = album_key
        n_favorited_tracks = len(favorited_tracks)
        n_tracks = tracks_in_album[album_id]
        print(f"Songs in {album_name} by {artists}: {n_tracks}. Favorited: {n_favorited_tracks}")
        # ratio = n_favorited_tracks / n_tracks
        # if ratio > FAVORITE_TO_SONGS_RATIO and n_tracks > MIN_SONGS:
        #     response = input("\tThis album has too many favorite songs. Process? [y/N] ")
        #     if response == 'y':
        #         sc.remove_songs_from_favorites(favorited_tracks)
        #         print("\tSongs removed from favorites!")
        #     else:
        #         print("\tSkipping this album per user request")
        # else:
        #     print("\tToo few favorite songs, skipping this album...")

